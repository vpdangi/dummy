import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatGridListModule, MatCardModule, MatMenuModule, MatIconModule} from '@angular/material';
import { MaterialModule} from './material';
import { AppComponent } from './app.component';
import { LeadDetailsComponent } from './Home/lead-details/lead-details.component';
import { AppRoutingModule,routingComponents } from './app-routing.module';
import { ContactComponent } from './contact/contact.component';
import { MyDishComponent } from './my-dish/my-dish.component';
import { LayoutModule } from '@angular/cdk/layout';
// import { LoginPageComponent } from './login-page/login-page.component';
// import { FormsModule } from '@angular/forms';
import { TableComponent } from './table/table.component';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './managment/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    LeadDetailsComponent,
    ContactComponent,
    routingComponents,// LeadDetailsDailogComponent,
    MyDishComponent,
    TableComponent,
    PageNotFoundComponent,
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MaterialModule,
    AppRoutingModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    LayoutModule,
    FormsModule,
    ScrollDispatchModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],

  entryComponents: [ContactComponent]
})
export class AppModule { }
