import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { LeadDetailsComponent } from '.home/lead-details/lead-details.component';
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
// import { LoginPageComponent } from './home/login-page/login-page.component';
import { LeadDetailsComponent } from './Home/lead-details/lead-details.component';
import { DeparmentComponent } from './managment/deparment/deparment.component';
import { EmployeeComponent } from './managment/employee/employee.component';
import { PageNotFoundComponent } from './managment/page-not-found/page-not-found.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
// import { MobileComponent } from './mobile/mobile.component';

export const  routes: Routes = [
    
    // { path: 'lead-details', component: LeadDetailsComponent },
    // { path: 'login-page', component: LoginPageComponent}
    // { path: '', component: DashboardComponent },
    { path: 'deparment', component: DeparmentComponent},
    {  path: 'employee', component: EmployeeComponent},
    { path: "**", component: PageNotFoundComponent},

    ]
    @NgModule({
        imports:[RouterModule.forRoot(routes)],
        exports:[RouterModule]
    })
    export class AppRoutingModule{}
export const routingComponents = [DeparmentComponent,
                                  EmployeeComponent,
                                  PageNotFoundComponent]