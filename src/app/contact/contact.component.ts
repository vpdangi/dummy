
import { Component, OnInit, Inject } from '@angular/core';
import { NgForm, FormGroup, FormArray, FormsModule, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MatSnackBar, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent, MatAutocompleteModule, MatAutocompleteTrigger } from '@angular/material';
// import { DataService } from 'src/app/service/data.service';
// import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  cookieValue;
  constructor(
    private formBuilder: FormBuilder,
    //  private dataService: DataService,
    public dialogRef: MatDialogRef<ContactComponent>,
    @Inject(MAT_DIALOG_DATA) public update_data: any, public snackBar: MatSnackBar) { }

    foods= [
      {value: 'steak-0', viewValue: 'Steak'},
      {value: 'pizza-1', viewValue: 'Pizza'},
      {value: 'tacos-2', viewValue: 'Tacos'}
    ];
  formStatus = false;
  saveFlag = true;
  empAddForm: FormGroup;
  updateValue;
  updateFlag = true;
  ngOnInit() {

    this.createForm();
  }

  private createForm() {

    this.empAddForm = this.formBuilder.group({

      first_name: new FormControl('', {
        validators: [Validators.required, Validators.maxLength(55)],
        updateOn: 'change'
      }),

      email: new FormControl(''),

      mobile: new FormControl(''),

      lead_source: new FormControl(''),

      call_back_date: new FormControl(''),

      response: new FormControl(''),

      lead_status: new FormControl(''),

      tenders_profile: new FormControl(''),

      tenders_exprience: new FormControl(''),
      
      description: new FormControl('') 

    });

    // if (this.updateValue !== undefined) {
    //   this.empAddForm.controls['first_name'].setValue(this.updateValue.first_name)
    //   this.empAddForm.controls['prefix'].setValue(this.updateValue.prefix)
    // }
  }

  getError() {
    return ' *This is required feild'
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: ['app-bottom-snackbar']
    });
  }
  empNameValid() {
    return " Empolyee Name  is Required Feild"
  }
  empEmailValid() {
    return " Enter  valid Email id"
  }

  empPhoneValid() {
    return " phone is Required Feild"
  }

  //---------------------------------------------add file end -------------------------------------
  onSubmit() {
    this.dialogRef.close();
  }
  cancel(): void {
    this.empAddForm.reset();
    this.dialogRef.close();
  }
}