import { Component} from '@angular/core';
import { FormControl,FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent {  

  ngOnInit(): void{
    this.profileName = new FormGroup({
      firstName:new FormControl(this.profileName.firstName,[
        Validators.required,
        Validators.minLength(4),
      ])
    })
  }
  profileName = this.fb.group({
      firstName :['', Validators.required],
      lastName : [''],
      address: this.fb.group({
      street: [''],
      city: [''],
      state:[''],
      zip: ['']
    })
  });
  status;
  constructor(private fb: FormBuilder){}
  onSubmit(){
    console.log(this.profileName.value)
  }
  // update(){
  //   this.profileName.patchValue({
  //     firstName : 'vishnu',
  //     address:{
  //       street: '123 f'
  //     }
  //   })
  // }
}
  


