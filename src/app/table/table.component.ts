import { Component , OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  

})
export class TableComponent  {
 
  data={
    "grade": "H030SG",
    "monthly_product": {
      "annual_plan": 200,
      "rolling_plan": 200,
      "process_order": 300
    },
    "mat_code": [
      1000000043,
      1000000045,
      1000000047,
      1000000318,
      10000023391,
      10000023395,
    ],
    "cat_chem": [
      "CHEM ZINC OXIDE",
      "CHEM ZINC OXIDE",
      "CHEM ZINC OXIDE",
      "CHEM ZINC OXIDE",
      "CHEM ZINC OXIDE",
      "CHEM ZINC OXIDE"
    ],
    "month": [
      [
        "Oct",
        "Nov",
        "Dec"
      ],
      [
        "Oct",
        "Nov",
        "Dec"
      ],
      [
        "Oct",
        "Nov",
        "Dec"
      ],
      [
        "Oct",
        "Nov",
        "Dec"
      ],
      [
        "Oct",
        "Nov",
        "Dec"
      ],
      [
        "Oct",
        "Nov",
        "Dec"
      ]
    ],
    "AOP": [
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ]
    ],
    "rolling_plan": [
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ]
    ],
    "project_consumption": [
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ],
      [
        30,
        29,
        33
      ]
    ],
    "lead": 90,
    "stock": 128,
    "in_transit": 90,
    "call_stagger": 90,
    "PR_released": 80,
    "PO_under_approval": 75
  }
  
    constructor() { }
    
    ngOnInit() {
    }
  
}