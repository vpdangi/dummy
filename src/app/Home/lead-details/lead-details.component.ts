import { Component, OnInit,Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ContactComponent } from 'src/app/contact/contact.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


export interface DialogData {
  name: string;
  animal: string;
}
@Component({
  selector: 'app-lead-details',
  templateUrl: './lead-details.component.html',
  styleUrls: ['./lead-details.component.css']
})
export class LeadDetailsComponent implements OnInit {
 
  name: string;
  animal: string;
 
  constructor(public dialog: MatDialog) { }
  ngOnInit() {
  }     
  
  detailsEmpolyee(): void {
    const dialogRef = this.dialog.open(ContactComponent, {
    width: '550px',
    data: { name: this.name , animal: this.animal}
    
    });
    
    dialogRef.afterClosed().subscribe(result => {
    console.log('The Dailog Was Closed') ;
    this.animal = result;
    });
    }
    }
    // @Component({
    //   selector: 'contact.component.dailog',
    //   templateUrl: 'contact.component.dailog.html',
    // })
    // export class LeadDetailsDailogComponent {
    
    //   constructor(
    //     public dialogRef: MatDialogRef<LeadDetailsDailogComponent>,
    //     @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
    
    //   onNoClick(): void {
    //     this.dialogRef.close();
    //   }
    
    // }





